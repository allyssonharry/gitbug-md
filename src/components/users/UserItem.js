import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Card, CardActionArea, CardContent, CardMedia, Grid, makeStyles, Typography } from '@material-ui/core'

const userItemStyles = makeStyles(theme => ({
  image: {
    height: '140px'
  },
  paper: {
    textAlign: 'center',
    fontSize: '1rem',
    fontWeight: 'bold'
  },
  link: {
    color: theme.inherits,
    textDecoration: 'none'
  }
}))

const UserItem = ({ user: { login, avatar_url } }) => {
  const style = userItemStyles()

  return (
    <Grid item xs={6} sm={3} lg={2}>
      <Card>
        <Link className={style.link} to={`/user/${login}`}>
          <CardActionArea href={null}>
            <CardMedia
              className={style.image}
              image={avatar_url}
              title=''
            />
            <CardContent>
              <Typography variant='h6' component='h6'>
                <div className={style.paper}>{login}</div>
              </Typography>
            </CardContent>
          </CardActionArea>
        </Link>
      </Card>
    </Grid>
  )
}

UserItem.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserItem
