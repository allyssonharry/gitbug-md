import React, { useContext, useState } from 'react'
import GithubContext from '../../context/github/githubContext'
import AlertContext from '../../context/alert/alertContext'
import { Button, makeStyles, TextField } from '@material-ui/core'

const searchStyles = makeStyles(theme => ({
  searchArea: {
    padding: '1.8rem 0'
  },
  form: {
    display: 'flex',
    flexDirection: 'row'
  },
  field: {
    marginRight: '6px'
  }
}))

const Search = () => {
  const githubContext = useContext(GithubContext)
  const alertContext = useContext(AlertContext)

  const [text, setText] = useState('')

  const onChange = e => {
    setText(e.target.value)
  }

  const onSubmit = e => {
    e.preventDefault()
    if (text === '') {
      alertContext.setAlert('Digite um nome de usuário antes', 'warning')
    } else {
      githubContext.searchUsers(text)
      setText('')
    }
  }

  const style = searchStyles()

  return (
    <div className={style.searchArea}>
      <form className={style.form} onSubmit={onSubmit}>
        <TextField
          type='text'
          name='text'
          placeholder='Digite o nome do usuário'
          value={text}
          onChange={onChange}
          className={style.field}
          fullWidth
          label='Buscar Usuário'
          variant='filled'
          //margin='dense'
        />
        <Button variant='contained' color='primary' type='submit'>Buscar</Button>
        {githubContext.users.length > 0 && (
          <Button variant='contained' style={{ marginLeft: '6px' }} onClick={githubContext.clearUsers}>Limpar</Button>
        )}
      </form>
    </div>
  )
}

export default Search
