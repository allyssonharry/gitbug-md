import React, { Fragment, useContext, useEffect } from 'react'
import { Link } from 'react-router-dom'
import GithubContext from '../../context/github/githubContext'
import Spinner from '../layout/Spinner'
import Repos from '../repos/Repos'
import { Card } from '@material-ui/core'

const User = ({ match }) => {
  const githubContext = useContext(GithubContext)

  const { getUserRepos, repos, getUser, loading, user } = githubContext

  useEffect(() => {
    getUser(match.params.login)
    getUserRepos(match.params.login)
    // eslint-disable-next-line
  }, [])

  const {
    name,
    avatar_url,
    location,
    bio,
    blog,
    login,
    html_url,
    company,
    followers,
    following,
    public_repos,
    public_gists,
    hireable,
  } = user

  if (loading) {
    return <Spinner />
  }

  return (
    <Fragment>

      <Link to='/'>Voltar ao início</Link>

      Hireable:
      {hireable ? (<i className='fas fa-check text-success' />) : (
        <i className='fas fa-times-circle text-danger' />)}

      <Card>
        <img src={avatar_url} className='rounded-circle' alt={''}
             style={{ width: '100px' }} />
        <h1>{name}</h1>
        <p>Location: {location}</p>
      </Card>
      <div>
        {bio && (
          <Fragment>
            <h3>Bio</h3>
            <p>{bio}</p>
          </Fragment>
        )}
        <a href={html_url} className='btn btn-dark my-1'>Visit Github
          Profile</a>
        <ul>
          <li>
            {login && <Fragment>
              <strong>Username: </strong> {login}
            </Fragment>}
          </li>
          <li>
            {company && <Fragment>
              <strong>Company: </strong> {company}
            </Fragment>}
          </li>
          <li>
            {blog && <Fragment>
              <strong>Website: </strong> {blog}
            </Fragment>}
          </li>
        </ul>
        <div className='card'>
          <div className='badge badge-primary'>Followers: {followers}</div>
          <div className='badge badge-primary'>Following: {following}</div>
          <div className='badge badge-primary'>Public
            Repos: {public_repos}</div>
          <div className='badge badge-primary'>Public
            Gists: {public_gists}</div>
        </div>
      </div>

      <Repos repos={repos} />
    </Fragment>
  )
}

export default User