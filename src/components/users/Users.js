import React, { useContext } from 'react'
import GithubContext from '../../context/github/githubContext'
import UserItem from './UserItem'
import Spinner from '../layout/Spinner'
import { makeStyles, Grid } from '@material-ui/core'

const usersLayoutStyle = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}))

const Users = () => {
  const githubContext = useContext(GithubContext)
  const { loading, users } = githubContext

  const style = usersLayoutStyle()

  if (loading) {
    return <Spinner />
  } else {
    return (
      <Grid container spacing={3} className={style.root}>
        {users.map(user => (
          <UserItem key={user.id} user={user} />
        ))}
      </Grid>
    )
  }
}

export default Users