import React from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { fade, makeStyles } from '@material-ui/core/styles'
import { AppBar, Container, Toolbar, Typography } from '@material-ui/core'

const navStyles = makeStyles(theme => ({
  toolbar: {
    padding: 0
  },
  title: {
    flexGrow: 1
  },
  navList: {
    listStyle: 'none'
  },
  navItem: {
    display: 'inline-block',
    '& a': {
      color: '#fff'
    },
    '&.active > a': {
      color: '#FFC107 !important'
    }
  },
  link: {
    color: theme.inherits,
    textDecoration: 'none',
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    fontWeight: 'bold',
    marginLeft: theme.spacing(2),
    padding: '6px 8px',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(2),
      width: 'auto',
    },
  }
}))

const Navbar = ({ icon, title, location }) => {
  const style = navStyles()
  return (
    <AppBar position="sticky">
      <Container>
        <Toolbar className={style.toolbar}>
          <Typography variant="h6" className={style.title}>
            <i className={icon} /> {title}
          </Typography>
          <ul className={style.navList}>
            <li className={location.pathname === '/' ? style.navItem + ' active' : style.navItem}>
              <Link className={style.link} to='/'>
                <i className="fas fa-home fa-lg" />
              </Link>
            </li>
            <li className={location.pathname.startsWith('/about') ? style.navItem + ' active' : style.navItem}>
              <Link className={style.link} to='/about'>
                <i className="fas fa-exclamation-circle fa-lg" />
              </Link>
            </li>
          </ul>
        </Toolbar>
      </Container>
    </AppBar>
  )
}

Navbar.defaultProps = {
  icon: 'fab fa-github fa-lg',
  title: 'Finder',
  location: '/'
}

Navbar.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  location: PropTypes.object.isRequired,
}

export default withRouter(Navbar)
