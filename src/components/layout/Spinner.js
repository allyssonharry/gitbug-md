import React, { Fragment } from "react";
import spinner from "./spinner.gif";

const spinnerStyles = {
  width: "50px",
  margin: "auto",
  display: "block"
};

const Spinner = () => (
  <Fragment>
    <img src={spinner} alt='Loading' style={spinnerStyles} />
  </Fragment>
);

export default Spinner;
