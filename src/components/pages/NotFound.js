import React from 'react'

const NotFound = () => {
  return (
    <div className="p-3 bg-light text-muted text-center">
      <h1>404</h1>
      <p className="lead">
        A página que você procura não foi encontrada.
      </p>
    </div>
  )
}

export default NotFound