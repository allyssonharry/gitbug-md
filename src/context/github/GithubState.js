import React, { useReducer } from 'react'
import axios from 'axios'
import GithubContext from '../github/githubContext'
import GithubReducer from '../github/githubReducer'
import { CLEAR_USERS, GET_REPOS, GET_USER, SEARCH_USERS, SET_LOADING } from '../types'

// Github Access token
let githubClientId
let githubClientSecret

// Limit repo results
let limit = '20'
let order = 'desc'

/** Check development type */
if (process.env.NODE_ENV !== 'production') {
  githubClientId = process.env.REACT_APP_GITHUB_CLIENT_ID
  githubClientSecret = process.env.REACT_APP_GITHUB_CLIENT_SECRET
} else {
  githubClientId = process.env.GITHUB_CLIENT_ID
  githubClientSecret = process.env.GITHUB_CLIENT_SECRET
}

const GithubState = props => {
  /** initial States */
  const initialState = {
    users: [],
    user: {},
    repos: [],
    loading: false,
    alert: null
  }

  const [state, dispatch] = useReducer(
    GithubReducer,
    initialState
  )

  /** Search users */
  const searchUsers = async text => {
    setLoading()

    const res = await axios.get(
      `https://api.github.com/search/users?q=${text}&client_id=${githubClientId}&client_secret=${githubClientSecret}`,
    )

    dispatch({
      type: SEARCH_USERS,
      payload: res.data.items
    })

    console.log(res.data.items)
  }

  /** Get single user */
  const getUser = async (username) => {
    setLoading()

    const res = await axios.get(
      `//api.github.com/users/${username}?client_id=${githubClientId}&client_secret=${githubClientSecret}`,
    )

    dispatch({
      type: GET_USER,
      payload: res.data
    })

    console.log(res)
  }

  /** Get user repository */
  const getUserRepos = async (username) => {
    setLoading()

    const res = await axios.get(
      `//api.github.com/users/${username}/repos?per_page=${limit}&sort=created:${order}&client_id=${githubClientId}&client_secret=${githubClientSecret}`,
    )

    dispatch({
      type: GET_REPOS,
      payload: res.data
    })
  }

  /** Set loading */
  const setLoading = () => dispatch({
    type: SET_LOADING
  })

  /** Clean users result */
  const clearUsers = () => dispatch({
    type: CLEAR_USERS
  })

  return (
    <GithubContext.Provider
      value={{
        users: state.users,
        user: state.user,
        repos: state.repos,
        loading: state.loading,
        alert: state.alert,
        searchUsers,
        clearUsers,
        getUser,
        getUserRepos
      }}
    >
      {props.children}
    </GithubContext.Provider>
  )
}

export default GithubState