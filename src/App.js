import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import GithubState from './context/github/GithubState'
import AlertState from './context/alert/AlertState'

import { Container } from '@material-ui/core'

import Navbar from './components/layout/Navbar'
import Alert from './components/layout/Alert'
import About from './components/pages/About'
import User from './components/users/User'
import Home from './components/pages/Home'
import NotFound from './components/pages/NotFound'

/** Import  */
//import './styles/index'

const App = () => {
  return (
    <GithubState>
      <AlertState>
        <Router>
          <div className='App'>
            <Navbar />
            <Container>
              <Alert alert={alert} />
              <Switch>
                <Route
                  exact
                  path='/'
                  component={Home}
                />
                {/* About page component */}
                <Route exact path='/about' component={About} />
                <Route exact path='/user/:login' component={User} />
                <Route
                  component={NotFound}
                />
              </Switch>
            </Container>
          </div>
        </Router>
      </AlertState>
    </GithubState>
  )
}

export default App